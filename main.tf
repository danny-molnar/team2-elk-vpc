resource "aws_vpc" "challenge-Vpc" {
  cidr_block = "192.168.0.0/16" 
  tags = {
    Name = "challenge week VPC"
  }
}

resource "aws_internet_gateway" "challenge-igw" {
  vpc_id = aws_vpc.challenge-Vpc.id

  tags = {
    Name = "IGW"
  }
}

resource "aws_route_table" "kibana-route-table" {
  vpc_id = aws_vpc.challenge-Vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.challenge-igw.id
  }

  tags = {
    Name = "kibana-RT"
  }
}

resource "aws_route_table" "monitoring-route-table" {
  vpc_id = aws_vpc.challenge-Vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.challenge-nat-gateway.id
  }

  tags = {
    Name = "monitoring-RT"
  }
}

resource "aws_route_table" "demo-route-table" {
  vpc_id = aws_vpc.challenge-Vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.challenge-nat-gateway.id
  }

  tags = {
    Name = "demo-RT"
  }
}

resource "aws_subnet" "kibana-subnet" {
  vpc_id     = aws_vpc.challenge-Vpc.id 
  cidr_block = "192.168.1.0/24"
  availability_zone = "eu-west-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "Kibana subnet"
  }
}

resource "aws_subnet" "monitoring-subnet" {
  vpc_id     = aws_vpc.challenge-Vpc.id 
  cidr_block = "192.168.2.0/24"
  availability_zone = "eu-west-1b"

  tags = {
    Name = "Monitoring subnet"
  }
}

resource "aws_subnet" "demo-subnet" {
  vpc_id     = aws_vpc.challenge-Vpc.id 
  cidr_block = "192.168.3.0/24"
  availability_zone = "eu-west-1c"

  tags = {
    Name = "Demo subnet"
  }
}

resource "aws_route_table_association" "kibana-route-table-association" {
  subnet_id      = aws_subnet.kibana-subnet.id
  route_table_id = aws_route_table.kibana-route-table.id
}

resource "aws_route_table_association" "monitoring-route-table-association" {
  subnet_id      = aws_subnet.monitoring-subnet.id
  route_table_id = aws_route_table.monitoring-route-table.id
}

resource "aws_route_table_association" "demo-route-table-association" {
  subnet_id      = aws_subnet.demo-subnet.id
  route_table_id = aws_route_table.demo-route-table.id
}

resource "aws_eip" "challenge-nat-gw-eip" {
  vpc      = true

  tags = {
    Name = "NAT GW EIP"
  }
}

resource "aws_nat_gateway" "challenge-nat-gateway" {
  allocation_id = aws_eip.challenge-nat-gw-eip.id
  subnet_id     = aws_subnet.kibana-subnet.id

  tags = {
    Name = "challenge-nat-gateway"
  }

  
  depends_on = [aws_internet_gateway.challenge-igw]
}

resource "aws_network_acl" "kibana-nacl" {
  vpc_id = aws_vpc.challenge-Vpc.id
  subnet_ids = [aws_subnet.kibana-subnet.id]

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }


  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = "kibana-nacl"
  }
}

resource "aws_network_acl" "monitoring-nacl" {
  vpc_id = aws_vpc.challenge-Vpc.id
  subnet_ids = [aws_subnet.monitoring-subnet.id]

 egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }


  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = "monitoring-nacl"
  }
}

resource "aws_network_acl" "demo-nacl" {
  vpc_id = aws_vpc.challenge-Vpc.id
  subnet_ids = [aws_subnet.demo-subnet.id]

   egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }


  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = "demo-nacl"
  }
}





