# challenge-Vpc README
*VPC*. [TEAM 2 | Talent Academy](https://cloudreach.jira.com/wiki/spaces/CE/pages/3955621898/Team+2+-+ELK+Stack)

## This Terraform code will create:
* Amazon AWS VPC
* Internet Gateway
* Nat Gateway
* 1 public Subnet to host the Kibana server/app
* 1 private Subnet to host both the Elasticsearch and Logstash servers/apps 
* 1 public Subnet to host the demo servers
* route tables have been created and associated to the subnets(one per subnet)
*  NACL's for each subnet

![Alt text](/files/ELK_Challange.png)

